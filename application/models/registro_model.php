<?php if (! defined('BASEPATH')) exit ('No direct script acces allowed');

class Registro_Model extends CI_Model {
    function __construct (){
        parent::__construct();
        $this->load->database();
    }
    
    function crearRegistro($data){

        $this->db->insert('datos',array('ape_paterno'=>$data['apaterno'],
                                         'ape_materno'=>$data['amaterno'],
                                         'nombre'=>$data['nombre'],
                                         'edad'=>$data['edad'],
                                         'procedencia'=>$data['procedencia'],
                                         'facultad'=>$data['facultad'],
                                         'carrera'=>$data['carrera'], 
                                         'area'=>$data['area'],
                                         'correo'=>$data['correo'],
                                         'id_ocupacion'=>$data['ocupacion']));
                                         
    }
    function getOcupacion(){
        
        // armamos la consulta
        $query = $this->db-> query('SELECT * FROM ocupacion');

        // si hay resultados
        if ($query->num_rows() > 0) {
            // almacenamos en una matriz bidimensional
            foreach($query->result() as $row)
                $arrDatos[htmlspecialchars($row->id_ocupacion, ENT_QUOTES)] = 
        htmlspecialchars($row->nombre, ENT_QUOTES);

            $query->free_result();
            return $arrDatos;
        }
        }

}

?>
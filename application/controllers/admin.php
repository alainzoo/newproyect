<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->helper('form');
		$this->load->model('registro_model');	
        $this->load->library(array('form_validation'));

    }
    public function index(){
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
		);
		$config = array(
			array(
					'field' => 'username',
					'label' => 'Nombre de Usuario',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar un %s.',
				),
			),
			array(
					'field' => 'password',
					'label' => 'Contraseña',
					'rules' => 'required',
					'errors' => array(
							'required' => 'Debe Ingresar una %s.',
					),
			)
		);		
	$this->load->model('Login_model');
	if($this->session->userdata('username')){
		$this->load->view('headers');
		$this->load->view('registro-admin');
		}

		$this->form_validation->set_rules($config);	
	
			if ($this->form_validation->run() == FALSE)
			{	
				$this->load->view('headers');
				$this->load->view('registro-admin');
			}
			else
			{
				
				if(isset($_POST['password']))
				{

					
					if($this->Login_model->login($_POST['username'], $_POST['password']))
					{
						
						$this->session->set_userdata('username',$_POST['username']);
						$result = $this->db->get('datos');
						$data = array('consulta'=>$result);
						$this->load->view('headers');
						$this->load->view('administrador', $data);

					}
					else
					{
						redirect('Admin');
					}
					
				
				}
			}
		
    } 
    function exportexcel(){
		if($this->session->userdata('username')){
			$result = $this->db->get('datos');
			$data = array('consulta'=>$result);
			$data['items'] = $items;
			$data['page_title'] = 'Listado de Registros';
			$data['filename'] = 'registros.xls';
			$this->load->view('exportexcel', $data);
		}
		else{
			redirect('Registro');
		}
	}
	function exportpdf(){	
		if($this->session->userdata('username')){		
			$result = $this->db->get('datos');
			$data = array('consulta'=>$result);
			$this->load->view('exportpdf',$data);
			$html = $this->output->get_output();
			$this->load->library('pdf');
			$this->dompdf->loadHtml($html);
			$this->dompdf->setPaper('A4', 'portrait');
			$this->dompdf->render();
			$this->dompdf->stream("exportpdf.pdf", array("Attachment"=>1));	
		}
		else{
			redirect('Registro');
		}	
	}   
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->helper('form');
		$this->load->model('registro_model');	
		$this->load->library(array('form_validation'));
	}

	public function index(){
		$datos['arrOcupacion'] = $this->registro_model->getOcupacion();
		$this->load->view('headers');
		$this->load->view('registro', $datos);
	
	}

	function cerrar(){
		$this->session->sess_destroy();
		redirect('Admin');
	}
	function registro(){	
		$data = array(
			'apaterno' => $this->input->post('apaterno'),
			'amaterno' => $this->input->post('amaterno'),
			'nombre' => $this->input->post('nombre'),
			'edad' => $this->input->post('edad'),
			'procedencia' => $this->input->post('procedencia'),
			'facultad' => $this->input->post('facultad'),
			'carrera' => $this->input->post('carrera'),
			'area' => $this->input->post('area'),
			'correo' => $this->input->post('correo'),
			'ocupacion' => $this->input->post('ocupacion')
		);
		$config = array(
			array(
					'field' => 'apaterno',
					'label' => 'Apellido Paterno',
					'rules' => 'required|alpha',
					'errors' => array(
						'required' => 'Debe Ingresar un %s.',
				),
			),
			array(
					'field' => 'amaterno',
					'label' => 'Apellido Materno',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar un %s.',
				),
			),
			array(
					'field' => 'nombre',
					'label' => 'Nombre',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar un %s.',
				),
			),
			array(
					'field' => 'edad',
					'label' => 'Edad',
					'rules' => 'required|numeric',
					'errors' => array(
						'required' => 'Debe Ingresar su %s.',
				),
			),
			array(
					'field' => 'procedencia',
					'label' => 'Procedencia',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar su %s.',
				),
			),
			array(
					'field' => 'facultad',
					'label' => 'Facultad',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar su %s.',
				),
			),
			array(
					'field' => 'carrera',
					'label' => 'Carrera',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar su %s.',
				),
			),
			array(
					'field' => 'area',
					'label' => 'Area',
					'rules' => 'required',
					'errors' => array(
						'required' => 'Debe Ingresar su %s.',
				),
			),
			array(
					'field' => 'correo',
					'label' => 'Correo',
					'rules' => 'required|valid_email',
					'errors' => array(
						'required' => 'Debe Ingresar su %s. o un Correo Valido',
				),
			),
			array(
					'field' => 'ocupacion',
					'label' => 'Ocupacion',
					'rules' => 'required|is_natural_no_zero',
					'errors' => array(
						'required|is_natural_no_zero' => 'Debe Ingresar una %s.',

				),
			),			
		);
		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == FALSE){
			$datos['arrOcupacion'] = $this->registro_model->getOcupacion();
			$this->load->view('headers');
			$this->load->view('registro', $datos);
		}
		else{
			$this->registro_model->crearRegistro($data);
			redirect('Registro');
		}
	}
}

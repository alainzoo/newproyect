<!DOCTYPE html>
<html>
    <body>
        <head>
            <meta charset="utf-8">
            <title>Registro UNACH</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/admin.css">
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        </head>
        <div id="menu">
        <ul>      
            <li><a href="<?php echo site_url("Admin/exportpdf")?>"><img height="50" width="50" src="<?php echo base_url(); ?>assets/image/pdf_icon.png"></a></li>
            <li><a href="<?php echo site_url("Admin/exportexcel")?>"><img height="50" width="50" src="<?php echo base_url(); ?>assets/image/excel_icon.jpg"></a></li>
            <li><a href="<?php echo site_url("Registro/cerrar")?>"><img height="50" width="50" src="<?php echo base_url(); ?>assets/image/logout_icon.jpg"></a></li>  
        </ul>
        </div>
        
        <div class="container box">
            <br />
            <h3>BIENVENIDO <?php echo $this->session->userdata('username'); ?></h3>
            <br />
            <div class="table">
                <table class="table">
                    <tr>
                    <th>Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Edad</th>
                    <th>Procedencia</th>
                    <th>Facultad</th>
                    <th>Carrera</th>
                    <th>Area</th>
                    <th>Correo</th>
                    <th>Ocupacion</th>
                    </tr>
                    <?php
                    foreach ($consulta->result() as $fila){ ?>
                    <tr>
                    <td width="150" height="50"><?php echo $fila->nombre; ?> </td>
                    <td width="150" height="50"><?php echo $fila->ape_paterno; ?> </td>
                    <td width="150" height="50"><?php echo $fila->ape_materno; ?> </td>
                    <td width="150" height="50"><?php echo $fila->edad; ?> </td>
                    <td width="150" height="50"><?php echo $fila->procedencia; ?> </td>
                    <td width="150" height="50"><?php echo $fila->facultad; ?> </td>
                    <td width="150" height="50"><?php echo $fila->carrera; ?> </td>
                    <td width="150" height="50"><?php echo $fila->area; ?> </td>
                    <td width="150" height="50"><?php echo $fila->correo; ?> </td>
                    <td width="150" height="50"><?php echo $fila->id_ocupacion; ?> </td>
                    </tr>
                    <?php 
                    }
                    ?>
                </table>
            </div>
        </div>
        <div id="space2" class="space">
        </div>
        <div id="space" class="space">
        </div>
        <div id="footer">
        </div>
    </body>
</html>

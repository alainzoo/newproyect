<?php defined('BASEPATH') OR exit('No direct script access allowed');
?>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/admin.css">
</head>
<div>
<ul id=”menu”>
    <?php if($this->session->userdata('username')): ?>
    <li><a href="<?php echo site_url("Admin/exportpdf")?>">Descargar PDF</a></li>
    <li><a href="<?php echo site_url("Admin/exportexcel")?>">Descargar Excel</a></li>
    <li><a href="<?php echo site_url("Registro/cerrar")?>">Cerrar Sesion</a></li>
       <?php else : ?>
       <li><a href="<?php echo site_url("Admin")?>">Iniciar Sesion</a></li>
    <?php endif; ?>
 </ul>
</div>


<!DOCTYPE html>
<html>
    <body>
        <head>
            <meta charset="utf-8">
            <title>Registro UNACH</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        </head>
        <div class="container box">
            <br />
            <h3 align="center">Listado de Registros</h3>
            <br />
            <div style="width:100%">
                <table class="table">
                    <tr>
                    <th style="height:35px; font-size:10px;">Nombre</th>
                    <th style="height:35px; font-size:10px;">Apellido Paterno</th>
                    <th style="height:35px; font-size:10px;">Apellido Materno</th>
                    <th style="height:35px; font-size:10px;">Edad</th>
                    <th style="height:35px; font-size:10px;">Procedencia</th>
                    <th style="height:35px; font-size:10px;">Facultad</th>
                    <th style="height:35px; font-size:10px;">Carrera</th>
                    <th style="height:35px; font-size:10px;">Area</th>
                    <th style="height:35px; font-size:10px;">Correo</th>
                    <th style="height:35px; font-size:10px;">Ocupacion</th>
                    </tr>
                    <?php
                    foreach ($consulta->result() as $fila){ ?>
                    <tr>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->nombre; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->ape_paterno; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->ape_materno; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->edad; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->procedencia; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->facultad; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->carrera; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->area; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->correo; ?> </td>
                    <td style="height:35px; font-size:10px;"><?php echo $fila->id_ocupacion; ?> </td>
                    </tr>
                    <?php 
                    }
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>

   
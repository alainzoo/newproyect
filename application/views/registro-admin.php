<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Logeo Administrador</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/main2.css">
	<style type="text/css">
		.login-title{
            font-weight: bolder;
            padding: 20px;
            text-align: center;
			color: #c7a462;
        }
        .respuesta{
            font-weight: bolder;
			color: #c7a462;
        }
	</style>
</head>
<body>
	<?php 
	if($this->session->userdata('username')){
		redirect('Registro/Cerrar');
		//deberia aqui, redirigir a control Admin pero sabiendo que el usuario esta logeado pero con flag para que
		//asi lo lleve hasta la view de administrador de nuevo ....
	}
	else{ 
	?>
		<div id="form" class="contenido">
			<div class="login-title">
				<h3>Administrador</h3>
			</div>
			<div class="respuesta">
            <p><?php echo validation_errors("*"); ?><p>
            </div>
			<form id="login" class="form-horizontal" method="POST" <?php echo form_open('Admin')?>>
			<div id="datos1" class="login">
					<label for="username" class="control-label">Email:</label>
					<input id="username" name="username" type="email" class="form-control" placeholder="Ingrese su email">
				</div>
				<div id="datos1" class="login">
					<label for="password" class="control-label">Contraseña:</label>
					<input id="password" name="password" type="password" class="form-control" placeholder="Ingrese Contraseña">
				</div><br>
				<div id="datos1" class="login">
					<input type="submit" class="btn btn-primary" value="Acceder">
				</div>
				
			</form>
			</div>
		</div>
	<?php 
	}	
	?>
		<div id="space2" class="space">
        </div>
        <div id="space" class="space">
        </div>
        <div id="footer">
        </div>
	<script src="<?= base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
	<script src="<?= base_url('')?>"></script>
</body>
</html>
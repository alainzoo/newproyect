<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/main2.css">
    <style type="text/css">
		.login-title{
            font-weight: bolder;
            padding: 20px;
            text-align: center;
			color: #c7a462;
        }
        .respuesta{
            font-weight: bolder;
			color: #c7a462;
        }
	</style>
</head>
<body>
        <div id="form" class="contenido">
			<div class="login-title">
                <h3>REGISTRO DE USUARIOS</h3>
            </div>
            <div class="respuesta">
            <p><?php echo validation_errors("*"); ?><p>
            </div>
            <?php echo form_open("Registro/registro") ?>
                <?php
                    $apaterno = array(
                        'type' => 'text',
                        'name' => 'apaterno',
                        'placeholder' => 'Apellido Paterno'
                    );
                    $amaterno = array(
                        'type' => 'text',
                        'name' => 'amaterno',
                        'placeholder' => 'Apellido Materno'
                    );
                    $nombre = array(
                        'type' => 'text',
                        'name' => 'nombre',
                        'placeholder' => 'Nombres'
                    );
                    $edad = array(
                        'type' => 'text',
                        'name' => 'edad',
                        'placeholder' => 'Edad'
                    );
                    $procedencia = array(
                        'type' => 'text',
                        'name' => 'procedencia',
                        'placeholder' => 'Institución de Procedencia'
                    );
                    $facultad = array(
                        'type' => 'text',
                        'name' => 'facultad',
                        'placeholder' => 'Facultad o Escuela'
                    );
                    $carrera = array(
                        'type' => 'text',
                        'name' => 'carrera',
                        'placeholder' => 'Carrera o Posgrado'
                    );
                    $area = array(
                        'type' => 'text',
                        'name' => 'area',
                        'placeholder' => 'Área de Conocimiento de Interés'
                    );
                    $correo = array(
                        'type' => 'email',
                        'name' => 'correo',
                        'placeholder' => 'Correo Electrónico'
                    );
                    $ocupacion = array(
                        'type' => 'text',
                        'name' => 'ocupacion'
                    );    
                ?>
                <div id="datos1" class="login">
                    <?php echo form_label('Apellido Paterno','apaterno') ?> <br/>      
                    <?Php echo form_input($apaterno)?>
                    <?php echo form_label('Apellido Materno','amaterno') ?><br/> 
                    <?Php echo form_input($amaterno)?> 
                    <?php echo form_label('Nombres','nombre') ?><br/> 
                    <?Php echo form_input($nombre)?>
                    <?php echo form_label('Edad','edad') ?><br/> 
                    <?Php echo form_input($edad)?>
                </div>
                <div id="datos2" class="login">
                    <?php echo form_label('Institución de procedencia','procedencia') ?><br/> 
                    <?Php echo form_input($procedencia)?><br/> 
                    <?php echo form_label('Facultad o Escuela','facultad') ?><br/> 
                    <?Php echo form_input($facultad)?><br/> 
                    <?php echo form_label('Carrera o Posgrado','carrera') ?><br/> 
                    <?Php echo form_input($carrera)?><br/> 
                    <?php echo form_label('Área de Conocimiento de Interés','area') ?><br/> 
                    <?Php echo form_input($area)?><br/> 
                    <?php echo form_label('Correo electrónico','correo') ?><br/> 
                    <?Php echo form_input($correo)?><br/>
                </div>
                <?php echo form_label('Seleccionar Ocupación ','ocupacion') ?><br/>
                <select id="select" name="ocupacion" class="login">
                <option selected>Seleccione una ocupación</option>
                    <?php
                    foreach ($arrOcupacion as $i => $ocupacion)
                        echo '<option value="',$i,'">',$ocupacion,'</option>'; 
                    ?>
                </select><br/><br/> 
                <div id="datos1" class="log">
                <input class="btn btn-primary" <?php echo form_submit('submit','Registrarse') ?>
                </div>
            <?php echo form_close() ?><br/> 
        </div>
        <div id="space2" class="space">
        </div>
        <div id="space" class="space">
        </div>
        <div id="footer">
        </div>

	<script src="<?= base_url('assets/js/jquery-1.11.3.min.js')?>"></script>
	<script src="<?= base_url('')?>"></script>
</body>
</html>